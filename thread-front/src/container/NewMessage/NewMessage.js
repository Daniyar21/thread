import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {createMessage} from "../../store/actions/messagesActions";

const NewMessage = () => {
    const [message, setMessage] = useState({
        author: '',
        message: '',
        image: null,
    });

    const submitFormHandler = async e => {
        if(message.author === ''){
            setMessage({...message, author: "Anonymous"})
        }

        e.preventDefault();

        const formData = new FormData();
        Object.keys(message).forEach(key => {
            formData.append(key, message[key]);
        });
        await dispatch(createMessage(formData));
    };

    const dispatch = useDispatch();

    const onInputChange = e => {
        const {name, value} = e.target;
        setMessage(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setMessage(prevState => {
            return {...prevState, [name]: file}
        });
    };


    return (
        <div>
            <input
                placeholder='Author'
                type='text'
                name='author'
                value={message.author}
                onChange={onInputChange}/>
            <p></p>
            <input
                placeholder='Message'
                type='text'
                name='message'
                value={message.message}
                onChange={onInputChange}/>
            <p></p>

            <input
                placeholder='image'
                type='file'
                name='image'
                onChange={fileChangeHandler}/>
            <button type='submit' onClick={submitFormHandler}>Send</button>
        </div>
    );
};

export default NewMessage;