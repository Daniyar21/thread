import React from 'react';
import './Message.css'
import {apiURL} from "../../config";

const Message = ({author, message,datetime, image}) => {
    let cardImage;

    if (image) {
        cardImage = apiURL + '/uploads/' + image;
    }

    return (
        <div className='message'>
            <h1>{author}</h1>
            <p>{message}</p>
            <p>{datetime}</p>
            <img src={cardImage} alt="something"/>


        </div>
    );
};

export default Message;