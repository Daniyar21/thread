import './App.css';
import NewMessage from "./container/NewMessage/NewMessage";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {fetchMessages} from "./store/actions/messagesActions";
import Message from "./components/Message/Message";

function App() {

  const messages = useSelector(state => state.messages);
  const dispatch=useDispatch();

  useEffect(() => {
      dispatch(fetchMessages());
  }, [dispatch])

  let allMessages;
  if(messages){
    allMessages = messages.map(m=>(
            <Message
                key={m.id}
                author={m.author}
                message={m.message}
                date={m.datetime}
                image={m.image}
            />
        )
    )
  }

  return (
      <div className="App">
        <NewMessage/>
        {allMessages}
      </div>
  );
}

export default App;

